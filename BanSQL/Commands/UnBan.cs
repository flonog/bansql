﻿using System.Text.RegularExpressions;
using BanSQL;
using Smod2;
using Smod2.Commands;

namespace Commands
{
    internal class Ban : ICommandHandler
    {
        private Main main;

        public Ban(Main main)
        {
            this.main = main;
        }

        public string GetCommandDescription()
        {
            return "Unban a player with IP or SteamID";
        }

        public string GetUsage()
        {
            return "SW_UNBAN <STEAMID | IP>";
        }

        public string[] OnCall(ICommandSender sender, string[] args)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("bsql_ban_enable", true))
            {
                if (args.Length >= 1)
                {
                    string Pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                    Regex checkIP = new Regex(Pattern);

                    if (checkIP.IsMatch(args[0]))
                    {
                        BanSQL.Lib.Ban banClass = new BanSQL.Lib.Ban(main);
                        banClass.UnBanPlayerByIP(args[0].Trim(' '));
                        return new string[] { LangManager.Manager.GetTranslation("command.success").Replace("\\n", "\n") };
                    }
                    else if (BanSQL.Lib.Misc.IsNumeric(args[0].Trim(' ')) && args[0].Length == 17)
                    {
                        BanSQL.Lib.Ban banClass = new BanSQL.Lib.Ban(main);
                        banClass.UnBanPlayerBySteamID(args[0].Trim(' '));
                        return new string[] { LangManager.Manager.GetTranslation("command.success").Replace("\\n", "\n") };
                    }
                    else
                    {
                        string msg = LangManager.Manager.GetTranslation("command.error").Replace("\\n", "\n");
                        msg = msg.Replace("[command]", "UNBAN");
                        msg = msg.Replace("[exception]", "Invalid IP or SteamID.");
                        return new string[] { msg };
                    }
                }
                else
                {
                    return new string[] { GetUsage() };
                }
            }
            else
            {
                return new string[] { LangManager.Manager.GetTranslation("ban.disabled") };
            }

        }
    }
}