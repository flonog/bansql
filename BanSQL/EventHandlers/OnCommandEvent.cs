﻿using System.Collections.Generic;
using System.Linq;
using BanSQL;
using BanSQL.Lib;
using Smod2;
using Smod2.EventHandlers;
using Smod2.Events;

namespace EventHandlers
{
    internal class OnCommandEvent : IEventHandlerCallCommand
    {
        private Main main;


        public OnCommandEvent(Main main)
        {
            this.main = main;
        }

        public void OnCallCommand(PlayerCallCommandEvent ev)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("bsql_warn_enable", true))
            {
                if (ev.Command.Trim(' ') == "warn")
                {
                    Warn warn = new Warn(main);

                    List<WarnObject> warnList = warn.GetWarnsFromSteamID(ev.Player.SteamId);

                    string msg = (warnList != new List<WarnObject>()) ? LangManager.Manager.GetTranslation("command.personnalWarn.warn").Replace("\\n", "\n") : LangManager.Manager.GetTranslation("command.personnalWarn.nowarn").Replace("\\n", "\n");

                    if (warnList.Any())
                    {
                        string listWarnString = "ID | REASON | ADMIN | DATE | EXPIRE DATE | ACTIVE\n-------------------------------------------------";
                        foreach (WarnObject warns in warnList)
                        {
                            listWarnString = $"{listWarnString}\n{warns.index} | {warns.warn} | {warns.admin} | {warns.dateTime.ToString("yyyy/MM/dd HH:mm:ss")} | {warns.expireTime.ToString("yyyy/MM/dd HH:mm:ss")} | {(warns.active ? LangManager.Manager.GetTranslation("general.yes") : LangManager.Manager.GetTranslation("general.no"))}\n-----------------------------------";
                        }

                        msg = msg.Replace("[number]", warnList.Count.ToString());
                        msg = msg.Replace("[nbactive]", warnList.Where(active => active.active == true).Count().ToString());
                        msg = msg.Replace("[warn]", listWarnString);

                        ev.ReturnMessage = msg.TrimEnd('-');
                    }
                    else
                    {
                        ev.ReturnMessage = LangManager.Manager.GetTranslation("command.personnalWarn.nowarn");
                    }
                }
            }
            else
            {
                ev.ReturnMessage = LangManager.Manager.GetTranslation("warn.disabled");
            }
        }
    }
}