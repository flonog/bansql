﻿using BanSQL;
using BanSQL.Lib;
using Smod2;
using Smod2.EventHandlers;
using Smod2.Events;

namespace EventHandlers
{
    internal class OnWaitingForPlayerEvent : IEventHandlerWaitingForPlayers
    {
        private Main main;

        public OnWaitingForPlayerEvent(Main main)
        {
            this.main = main;
        }

        public void OnWaitingForPlayers(WaitingForPlayersEvent ev)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("bsql_warn_enable", true))
            {
                Warn warnClass = new Warn(main);
                warnClass.CreateTable();
            }

            if (ConfigManager.Manager.Config.GetBoolValue("bsql_ban_enable", true))
            {
                Ban banClass = new Ban(main);
                banClass.CreateTable();
            }
        }
    }
}