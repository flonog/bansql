﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Smod2;
using Smod2.API;

namespace BanSQL.Lib
{
    class Warn
    {


        public MySqlConnection connection;
        public string ipaddress = ConfigManager.Manager.Config.GetStringValue("bsql_sql_ipaddress", "127.0.0.1").TrimEnd(' ').TrimStart(' ');
        public string username = ConfigManager.Manager.Config.GetStringValue("bsql_sql_username", "root").TrimEnd(' ').TrimStart(' ');
        public string password = ConfigManager.Manager.Config.GetStringValue("bsql_sql_password", string.Empty).TrimEnd(' ').TrimStart(' ');
        public string database = ConfigManager.Manager.Config.GetStringValue("bsql_sql_database", "scpsl").TrimEnd(' ').TrimStart(' ');

        public string warntable = ConfigManager.Manager.Config.GetStringValue("bsql_sql_warntable", "warn").TrimEnd(' ').TrimStart(' ');

        Plugin plugin;

        public Warn(Plugin plugin)
        {
            this.plugin = plugin;
            this.InitConnection();
        }

        private void InitConnection()
        {
            string connectionString = $"SERVER={ipaddress}; DATABASE={database}; UID={username}; PASSWORD={password}";
            this.connection = new MySqlConnection(connectionString);
        }

        public void CreateTable()
        {
            try
            {
                connection.Open();

                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"CREATE TABLE IF NOT EXISTS {warntable} (`id` INT(11) NOT NULL AUTO_INCREMENT,`steamid` VARCHAR(17) NOT NULL,`warn` LONGTEXT NOT NULL,`admin` VARCHAR(32) NOT NULL,`datetime` DATETIME NOT NULL,`expireTime` DATETIME NOT NULL, PRIMARY KEY (`id`))COLLATE='latin1_swedish_ci'ENGINE=InnoDB;";
                command.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception");
                msg = msg.Replace("[exception]", $"Error when creating table for warn.\n{e}");
                plugin.Error(msg);
            }
            finally
            {
                connection.Close();
            }
        }

        public void AddWarn(string steamid, string warn, string admin)
        {
            try
            {
                if (warn.Trim(' ') == "") { return; }

                admin = admin.Trim(' ') == "" ? "Unknown admin" : admin;

                if (steamid.Length != 17) { return; };

                connection.Open();

                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"INSERT INTO {warntable} (steamid, warn, admin, datetime, expireTime) VALUES (@steamid, @warn, @admin, @datetime, @expireTime)";
                command.Parameters.AddWithValue("@steamid", steamid);
                command.Parameters.AddWithValue("@warn", warn);
                command.Parameters.AddWithValue("@admin", admin);
                command.Parameters.AddWithValue("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                command.Parameters.AddWithValue("@expireTime", DateTime.Now.Add(StringToDateTime.ParseStringDuration(ConfigManager.Manager.Config.GetStringValue("bsql_warn_activedurate", "1M"))).ToString("yyyy-MM-dd HH:mm:ss"));
                command.ExecuteNonQuery();
                connection.Close();

                List<WarnObject> warnsActive = GetWarnsFromSteamID(steamid).Where(owo => owo.active == true).ToList();

                if (warnsActive.Count >= ConfigManager.Manager.Config.GetIntValue("bsql_warn_autoban", 5))
                {
                    foreach(Player p in PluginManager.Manager.Server.GetPlayers())
                    {
                        if(p.SteamId == steamid)
                        {
                            p.Ban(StringToDateTime.ParseStringDuration(ConfigManager.Manager.Config.GetStringValue("bsql_warn_autoban_duration", "50y")).Minutes, LangManager.Manager.GetTranslation("warn.autoban.banMessage").Replace("\\n", "\n"));
                        }
                    }
                }

                string msg = LangManager.Manager.GetTranslation("warn.broadcast.playerWarned");

                try
                {
                    msg = msg.Replace("[player]", Misc.GetNameBySteamID(steamid));
                }
                catch (Exception)
                {
                    msg = msg.Replace("[player]", steamid);
                }

                msg = msg.Replace("\\n", "\n");
                msg = msg.Replace("[admin]", admin);
                msg = msg.Replace("[warn]", warn);

                PluginManager.Manager.Server.Map.Broadcast(5, msg, true);

            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception");
                msg = msg.Replace("[exception]", $"The warn {warn} for {steamid} by {admin} doesn't work.\n {e}");
                plugin.Error(msg);
            }
            finally
            {
                connection.Close();
            }
        }

        public void DelWarn(int index)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();

                command.CommandText = $"DELETE FROM `{warntable}` WHERE  `id`= {index};";

                command.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception");
                msg = msg.Replace("[exception]", $"Cannot delete warn's index {index}.\n {e}");
                plugin.Error(msg);
            }
            finally
            {
                connection.Close();
            }
        }

        public List<WarnObject> GetWarnsFromSteamID(string steamid)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT * FROM {warntable} WHERE steamid = {steamid};";
                var dr = command.ExecuteReader();
                List<WarnObject> warnlist = new List<WarnObject>();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        int index = dr.GetInt32(0);
                        string warn = dr.GetString(2);
                        string admin = dr.GetString(3);
                        DateTime dateTime = dr.GetDateTime(4);
                        DateTime expireTime = dr.GetDateTime(5);

                        warnlist.Add(new WarnObject(index, steamid, warn, admin, dateTime, expireTime));
                    }
                }
                connection.Close();
                return warnlist;
            }
            catch(Exception e)
            {
                string msg = plugin.GetTranslation("general.exception");
                msg = msg.Replace("[exception]", $"Cannot get warn for the steamID : {steamid}.\n {e}");
                plugin.Error(msg);
                connection.Close();
                return new List<WarnObject>();
            }
            finally
            {
                connection.Close();
            }
        }
    }

    public class WarnObject
    {

        public int index { get; set; }
        public string steamid { get; set; }
        public string warn { get; set; }
        public string admin { get; set; }
        public DateTime dateTime;
        public DateTime expireTime;
        public bool active;

        public WarnObject(int index, string steamid, string warn, string admin, DateTime dateTime, DateTime expireTime)
        {
            this.index = index;
            this.steamid = steamid;
            this.warn = warn;
            this.admin = admin;
            this.dateTime = dateTime;
            this.expireTime = expireTime;

            this.active = (expireTime > dateTime) ? true : false;
        }
    }
}
